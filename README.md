# *OS*-configuration
*****

This **repo** is all `configuration` files collection of my `os`.

## Content

- Xdefault
- Xresource
- Xmodmap
- xinitrc
- xprofile
- Zshrc